players_with_hi_priv = {}

minetest.register_privilege("hi", {
	description = "Allows the player to make use of auto hi mod.",
})

minetest.register_on_joinplayer(function(player)
	local name = player:get_player_name()
	print(name)
	if minetest.check_player_privs(name, {hi = true}) then
		table.insert(players_with_hi_priv, name)
		minetest.chat_send_all("Give a warm welcome to " .. name)
	else
		for _, pplayer_name in ipairs(players_with_hi_priv) do
			local player = minetest.get_player_by_name(pplayer_name)
			local custom_msg = string.gsub(player:get_attribute("custom_hi_msg") or "Hello |!", "(|)", name)
			minetest.chat_send_all("[" .. pplayer_name .. "] " .. custom_msg)
		end

	end

end)

minetest.register_chatcommand("set_hi", {
	params = "<message>",
	description = "Sets the auto hi message.",
	privs = {hi = true},
	func = function(name, params)
		local player = minetest.get_player_by_name(name)
		player:set_attribute("custom_hi_msg", params)
	end
})
